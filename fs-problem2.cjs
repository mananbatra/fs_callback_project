const fs = require('fs');
const path = require('path');
const readFileDir='/home/manan/Downloads'
const fileNamePath='./lipsum.txt';
const filePath=path.resolve(readFileDir,fileNamePath);
const dirPath='./';

console.log();

function ReadData() {
    fs.readFile(filePath, 'utf8', (error, data) => {

        if (error) {
            console.error(error);
        } else {
            console.log("data read successfully");
            writeNewFile(dirPath, data)
        }
    });

    function writeNewFile(dirPath, data) {
        const fileName = `file_${Math.floor(Math.random() * 100)}.txt`;
        const firstFilePath = path.join(dirPath, fileName);

        let upperCaseData = data.toUpperCase();
        fs.writeFile(firstFilePath, JSON.stringify(upperCaseData), (error) => {
            if (error) {
                console.error(error);
            }
            else {
                console.log(`${fileName} created SuccessFully`);

            }
        });

        const fileNameStore = `filenames.txt`;
        const filePathStore = path.join(dirPath, fileNameStore);

        fs.writeFile(filePathStore, (JSON.stringify(firstFilePath) + "\n"), (error) => {
            if (error) {
                console.error(error);
            }
            else {
                console.log(`${fileNameStore} created SuccessFully and stored ${firstFilePath}`);
                readAndSplit(firstFilePath, dirPath, filePathStore);
            }
        });

    }

    function readAndSplit(firstFilePath, dirPath, filePathStore) {
        fs.readFile(firstFilePath, 'utf8', (error, data) => {

            if (error) {
                console.error(error);
            } else {
                console.log("data read successfully from " + firstFilePath);

                let lowerCaseData = data.toLowerCase().split('\n');
                createNewFile(dirPath, filePathStore, lowerCaseData)
            }
        });
    }

    function createNewFile(dirPath, filePathStore, data) {
        const fileName = `file_${Math.floor(Math.random() * 100)}.txt`;
        const newFilePath = path.join(dirPath, fileName);


        fs.writeFile(newFilePath, JSON.stringify(data), (error) => {
            if (error) {
                console.error(error);
            }
            else {
                console.log(`${fileName} created SuccessFully`);

            }
        });

        fs.appendFile(filePathStore, (JSON.stringify(newFilePath) + "\n"), (error) => {
            if (error) {
                console.error(error);
            }
            else {
                console.log(`${filePathStore} appended SuccessFully`);
                readAndSort(newFilePath, filePathStore, dirPath)
            }
        });

    }

    function readAndSort(newFilePath, filePathStore, dirPath) {
        fs.readFile(newFilePath, 'utf8', (error, splitedData) => {

            if (error) {
                console.error(error);
            } else {
                console.log("data read successfully from " + newFilePath);

                let sortedData = JSON.parse(splitedData).sort();
                createNewSortFile(dirPath, filePathStore, sortedData)
            }
        });
    }

    function createNewSortFile(dirPath, filePathStore, sortedData) {
        const fileName = `file_${Math.floor(Math.random() * 100)}.txt`;
        const newSortFilePath = path.join(dirPath, fileName);


        fs.writeFile(newSortFilePath, JSON.stringify(sortedData), (error) => {
            if (error) {
                console.error(error);
            }
            else {
                console.log(`${fileName} created SuccessFully`);

            }
        });

        fs.appendFile(filePathStore, JSON.stringify(newSortFilePath), (error) => {
            if (error) {
                console.error(error);
            }
            else {
                console.log(`${filePathStore} appended SuccessFully`);
                readAndDelete(filePathStore, dirPath)
            }
        });

    }

    function readAndDelete(fileNameStore, dirPath) {

        fs.readFile(fileNameStore, 'utf8', (error, fileNameList) => {

            if (error) {
                console.error(error);
            } else {
                console.log("data read successfully from " + fileNameStore);

                let fileNameArray = fileNameList.split("\n");
                //console.log(fileNameArray);
                deleteFiles(fileNameArray, dirPath)

            }
        });

    }
    function deleteFiles(fileNameArray, dirPath) 
    {
        fileNameArray.map(file => {
            const deleteFilePath = path.join(dirPath , JSON.parse(file));
            //console.log(deleteFilePath);
            return fs.unlink(deleteFilePath, (err) => {
                if (err) {
                    console.error(err)
                } else {
                    console.log("File Successfully Deleted")
                }
            });
        })
    }
}

module.exports = ReadData;
