const { error } = require('console');
const fs = require('fs');
const path = require('path');

function makeDir(dirPath, numberOfFiles) {
    fs.mkdir(dirPath, (error) => {
        if (error) {
            console.error(error);
        } else {
            console.log("Directory Created Successfully");
            createRandomFiles(dirPath, numberOfFiles);
        }
    });

    function createRandomFiles(dirPath, numberOfFiles) {
        //console.log(numberOfFiles);
        let fileArray = [];
        for (let index = 1; index <= numberOfFiles; index++) {
            const fileName = `file_${index}.json`;
            const filePath = path.join(dirPath, fileName);

            fs.writeFile(filePath, '', (error) => {
                if (error) {
                    console.error(error);
                } else {
                    fileArray.push(`file_${index}.json`);
                    console.log("file created");

                    if (fileArray.length == numberOfFiles) {
                        deleteFiles(null, dirPath, fileArray)
                    }
                }
            });
            //console.log(fileArray.length);

        }

        function deleteFiles(error, dirPath, fileArray) {
            if (error) {
                console.error(error);
            }
            else {
                for (let index = 0; index < fileArray.length; index++) {

                    const filePath = path.join(dirPath, fileArray[index])
                    fs.unlink(filePath, function (error) {
                        if (error) {
                            console.log(error);
                        } else {
                            console.log("File deleted");
                        }
                    })
                }
            }
        }
        //console.log(fileArray);
    }
}

module.exports = makeDir;
